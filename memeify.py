import discord
import coloredlogs
import logging
import random
import os
from dotenv import load_dotenv
from emojis import emojis

load_dotenv()
log = logging.getLogger(__name__)
coloredlogs.install(level=os.getenv('LOG_LEVEL'))
client = discord.Client()


@client.event
async def on_ready():
    log.info(f'Logged in as user {client.user}')


@client.event
async def on_message(message):
    if message.author == client.user:
        return

    if message.content.startswith('!meme '):
        log.info(f'Request to memeify: {message.content}')
        meme_text = []
        for char in message.content[len('!meme '):]:
            if char == ' ':
                meme_text.append(f' {random.choice(emojis)} ')
            meme_text.append(char)
        await message.channel.send(''.join(meme_text))

client.run(os.getenv('TOKEN'))
